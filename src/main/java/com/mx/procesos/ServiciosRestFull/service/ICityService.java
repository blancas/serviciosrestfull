package com.mx.procesos.ServiciosRestFull.service;
import java.util.List;
import com.mx.procesos.ServiciosRestFull.bean.City;
public interface ICityService 
{	
    public List<City> findAll();
}

