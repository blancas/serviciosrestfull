package com.mx.procesos.ServiciosRestFull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiciosRestFullApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiciosRestFullApplication.class, args);
	}

}
